<?php

namespace Ngay4\Internship\Plugin;

use Magento\Checkout\Controller\Cart\Add;

class Subtotal extends Add
{

    protected function goBack($backUrl = null, $product = null)
    {
        if (!$this->getRequest()->isAjax()) {
            return parent::_goBack($backUrl);
        }
        $total["subtotal"] = $this->cart->getQuote()->getSubtotal();
        $this->getResponse()->representJson(
            $this->_objectManager->get(\Magento\Framework\Json\Helper\Data::class)->jsonEncode($total)
        );
    }
}
