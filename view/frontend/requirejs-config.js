var config = {
    "map" : {
        "*" : {
            'owl.carousel': "Ngay4_Internship/js/owl.carousel",
            'widgetCarousel': "Ngay4_Internship/js/widgetowlcarousel",
            'catalogAddToCart': "Ngay4_Internship/js/catalog-add-to-cart",
        }
    },
    shim: {
        'owl.carousel': {
            deps: ['jquery']
        },
        'widgetCarousel': {
            deps: ['jquery']
        }
    }
};
